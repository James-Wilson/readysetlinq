﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Data;
using CoreTechs.Common.Text;
using System;

namespace DataManager
{
    public partial class FormDataManager : Form
    {

        private string _filePath = string.Empty;

        public FormDataManager()
        {
            InitializeComponent();
        }

        // Load the contents of a file from the given path
        public DataTable GetDataTableFromCsv(bool isFirstRowHeader = true)
        {
            try
            {

                if (File.Exists(_filePath))
                {
                    using StreamReader r = new StreamReader(_filePath);
                    TextReader reader = new StringReader(r.ReadToEnd());
                    using DataTable table = new DataTable();
                    using IEnumerator<Record> it = reader.ReadCsvWithHeader().GetEnumerator();
                    if (!it.MoveNext()) return new DataTable();

                    foreach (string k in it.Current.Keys)
                        table.Columns.Add(k);

                    do
                    {
                        DataRow row = table.NewRow();
                        foreach (string k in it.Current.Keys)
                            row[k] = it.Current[k];

                        table.Rows.Add(row);

                    } while (it.MoveNext());

                    return table;
                }
                else
                {
                    return new DataTable();
                }
            }
            catch
            {
                return new DataTable();
            }
        }

        public void SaveDataTable(DataTable dataTable)
        {
            // Make sure a path was given
            if (string.IsNullOrWhiteSpace(_filePath))
                return;
            // Save the data to a CSV
            dataTable.WriteToCsvFile(_filePath);
        }

        private void btnLoad_Click(object sender, System.EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "CSV files (*.csv, *.tsv)|*.csv;",
                FilterIndex = 0,
                Multiselect = false
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _filePath = openFileDialog.FileName;
                if (!string.IsNullOrEmpty(_filePath))
                {
                    dataGridManage.Columns.Clear();
                    dataGridManage.DataSource = GetDataTableFromCsv();
                    foreach (DataGridViewColumn column in dataGridManage.Columns)
                    {
                        column.SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                    this.Text = String.Format("ReadySetLinq - DataManager [{0}]", _filePath);
                }

            }
        }

        private void dataGridManage_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dataGridManage.RemoveEmptyRows();
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            dataGridManage.RemoveEmptyRows();
            SaveDataTable((DataTable)dataGridManage.DataSource);
        }
    }
}
