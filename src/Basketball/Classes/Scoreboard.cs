﻿namespace ReadySetLinq.Basketball
{

    enum Teams
    {
        None = 0,
        Home = 1,
        Away = 2
    }

    class Scoreboard
    {
        #region Variables
        // Home Data
        private int _homePoints = 0;

        // Away Data
        private int _awayPoints = 0;

        // General Data
        private Teams _timeout = Teams.None;
        private Teams _substitution = Teams.None;

        // Infobox
        private int _infoIndex = 0;
        private int _infoLastEdited = 0;

        #endregion

        // Reset all of the scoreboard data
        public void Reset()
        {
            _homePoints = 0;
            _awayPoints = 0;
            _timeout = Teams.None;
            _substitution = Teams.None;
            _infoLastEdited = 0;
        }

        public void ResetPoints()
        {
            _homePoints = 0;
            _awayPoints = 0;
        }

        public int AddPoint(Teams team = Teams.None, int amount = 1)
        {
            int _response = 0;
            switch (team)
            {
                case Teams.Home:
                    _homePoints += amount;
                    _response = _homePoints;
                    break;
                case Teams.Away:
                    _awayPoints += amount;
                    _response = _awayPoints;
                    break;
                default:
                    break;
            }

            return _response;
        }

        public int RemovePoint(Teams team = Teams.None, int amount = 1)
        {
            int _response = 0;
            switch (team)
            {
                case Teams.Home:
                    if (_homePoints > 0)
                        _homePoints -= amount;
                    _response = _homePoints;
                    break;
                case Teams.Away:
                    if (_awayPoints > 0)
                        _awayPoints -= amount;
                    _response = _awayPoints;
                    break;
                default:
                    break;
            }

            return _response;
        }



        public int HomePoints
        {
            get { return _homePoints; }
            set { _homePoints = value; }
        }

        public int AwayPoints
        {
            get { return _awayPoints; }
            set { _awayPoints = value; }
        }

        public Teams Timeout
        {
            get { return _timeout; }
        }

        public Teams Substitution
        {
            get { return _substitution; }
        }

        public int InfoIndex
        {
            get { return _infoIndex; }
            set { _infoIndex = value; }
        }

        public int InfoLastEdited
        {
            get { return _infoLastEdited; }
            set { _infoLastEdited = value; }
        }
    }
}
