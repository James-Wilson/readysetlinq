﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadySetLinq.Volleyball
{
    enum Teams
    {
        None = 0,
        Home = 1,
        Away = 2
    }

    class Scoreboard
    {
        #region Variables
        // Home Data
        private int _homePoints = 0;
        private int _homeSets = 0;
        private int _homeRedCards = 0;
        private int _homeYellowCards = 0;

        // Away Data
        private int _awayPoints = 0;
        private int _awaySets = 0;
        private int _awayRedCards = 0;
        private int _awayYellowCards = 0;

        // General Data
        private SetResults _setResults = new SetResults();
        private Teams _serving = Teams.None;
        private Teams _timeout = Teams.None;
        private Teams _substitution = Teams.None;

        // Infobox
        private int _infoIndex = 0;
        private int _infoLastEdited = 0;

        #endregion

        // Reset all of the scoreboard data
        public void Reset()
        {
            _homePoints = 0;
            _homeSets = 0;
            _homeRedCards = 0;
            _homeYellowCards = 0;
            _awayPoints = 0;
            _awaySets = 0;
            _awayRedCards = 0;
            _awayYellowCards = 0; 
            _setResults = new SetResults();
            _serving = Teams.None;
            _timeout = Teams.None;
            _substitution = Teams.None;
            _infoLastEdited = 0;
        }

        public void ResetPoints()
        {
            _homePoints = 0;
            _awayPoints = 0;
        }
        
        public int SetPoint(Teams team = Teams.None, bool increase = true)
        {
            int _response = 0;
            switch (team)
            {
                case Teams.Home:
                    if (increase)
                        _homePoints++;
                    else
                    {
                        if (_homePoints > 0)
                            _homePoints--;
                    }
                    _response = _homePoints;
                    break;
                case Teams.Away:
                    if (increase)
                        _awayPoints++;
                    else
                    {
                        if (_awayPoints > 0)
                            _awayPoints--;
                    }
                    _response = _awayPoints;
                    break;
                default:
                    break;
            }
            _serving = team;

            return _response;
        }

        public int SetWin(Teams team = Teams.None, bool increase = true, bool resetPoints = false)
        {
            int _response = 0;

            switch (team)
            {
                case Teams.Home:
                    if (increase)
                        _homeSets++;
                    else
                    {
                        if (_homeSets > 0)
                            _homeSets--;
                    }
                    _response = _homeSets;
                    break;
                case Teams.Away:
                    if (increase)
                        _awaySets++;
                    else
                    {
                        if (_awaySets > 0)
                            _awaySets--;
                    }
                    _response = _awaySets;
                    break;
                default:
                    break;
            }
            _serving = Teams.None;

            if (resetPoints)
            {
                _homePoints = 0;
                _awayPoints = 0;
            }

            return _response;
        }

        public int GetSetNumber()
        {
            return 1 + (_homeSets + _awaySets);
        }

        public (int, int) UpdateSetResults(int setNumber)
        {
            (int, int) _results = (_homePoints, _awayPoints);

            switch (setNumber)
            {
                case 1:
                    _setResults.SetOne(_results.Item1, _results.Item2);
                    break;
                case 2:
                    _setResults.SetTwo(_results.Item1, _results.Item2);
                    break;
                case 3:
                    _setResults.SetThree(_results.Item1, _results.Item2);
                    break;
                case 4:
                    _setResults.SetFour(_results.Item1, _results.Item2);
                    break;
                case 5:
                    _setResults.SetFive(_results.Item1, _results.Item2);
                    break;
            }

            return _results;
        }

        public void ResetSetResults()
        {
            _setResults.Reset();
        }

        public int SetServing(Teams team = Teams.None)
        {
            _serving = team;
            return (int)_serving;
        }

        public int RedCard(Teams team = Teams.None, bool increase = true)
        {
            int _response = 0;
            switch (team)
            {
                case Teams.Home:
                    if (increase)
                        _homeRedCards++;
                    else
                    {
                        if (_homeRedCards > 0)
                            _homeRedCards--;
                    }
                    _response = _homeRedCards;
                    break;
                case Teams.Away:
                    if (increase)
                        _awayRedCards++;
                    else
                    {
                        if (_awayRedCards > 0)
                            _awayRedCards--;
                    }
                    _response = _awayRedCards;
                    break;
                default:
                    break;
            }

            return _response;
        }

        public int YellowCard(Teams team = Teams.None, bool increase = true)
        {
            int _response = 0;
            switch (team)
            {
                case Teams.Home:
                    if (increase)
                        _homeYellowCards++;
                    else
                    {
                        if (_homeYellowCards > 0)
                            _homeYellowCards--;
                    }
                    _response = _homeYellowCards;
                    break;
                case Teams.Away:
                    if (increase)
                        _awayYellowCards++;
                    else
                    {
                        if (_awayYellowCards > 0)
                            _awayYellowCards--;
                    }
                    _response = _awayYellowCards;
                    break;
                default:
                    break;
            }

            return _response;
        }

        public (int, int) GetSetResult(int setNumber)
        {
            return _setResults.GetSetResult(setNumber);
        }

        public Teams GetSetWinner(int setNumber)
        {
            return _setResults.GetSetWinner(setNumber);
        }

        public void UpdateSet(int setNumber, Teams team, bool increase)
        {
            switch(setNumber)
            {
                case 1:
                    if (team == Teams.Home)
                    {

                    }
                    break;
            }
        }

        public int HomePoints
        {
            get { return _homePoints; }
            set { _homePoints = value; }
        }

        public int HomeSets
        {
            get { return _homeSets; }
            set { _homeSets = value; }
        }

        public int HomeRedCards
        {
            get { return _homeRedCards; }
        }

        public int HomeYellowCards
        {
            get { return _homeYellowCards; }
        }

        public int AwayPoints
        {
            get { return _awayPoints; }
            set { _awayPoints = value; }
        }

        public int AwaySets
        {
            get { return _awaySets; }
            set { _awaySets = value; }
        }

        public int AwayRedCards
        {
            get { return _awayRedCards; }
        }

        public int AwayYellowCards
        {
            get { return _awayYellowCards; }
        }

        public Teams Serving
        {
            get { return _serving; }
        }

        public Teams Timeout
        {
            get { return _timeout; }
        }

        public Teams Substitution
        {
            get { return _substitution; }
        }

        public int InfoIndex
        {
            get { return _infoIndex; }
            set { _infoIndex = value; }
        }

        public int InfoLastEdited
        {
            get { return _infoLastEdited; }
            set { _infoLastEdited = value; }
        }
    }
}
