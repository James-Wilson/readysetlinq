﻿using System;
using System.Collections.Generic;
using XPression;

namespace ReadySetLinq.Shared
{
    public class Xpression
    {
        xpEngine _engine;

        public Xpression()
        {
            try
            {
                _engine = new xpEngine();
            } catch { }
        }

        #region TakeItems
        public bool SetTakeItemOnline(int takeID)
        {
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        return takeItem.Execute();
                    }
                }
            }
            catch { return false; }

            return true;
        }

        public bool SetTakeItemOffline(int takeID)
        {
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        return takeItem.SetOffline();
                    }
                }
            }
            catch { return false; }

            return true;
        }

        public bool GetTakeItemStatus(int takeID)
        {
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item 
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        return takeItem.IsOnline;
                    }
                }
            }
            catch { return false; }

            return false;
        }

        public xpTakeItem GetTakeItem(int takeID)
        {
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item 
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        return takeItem;
                    }
                }
            }
            catch { return null; }

            return null;
        }

        public int GetTakeItemLayer(int takeID)
        {
            int layer = 0;
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item 
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        return takeItem.Layer;
                    }
                }
            }
            catch { return layer; }

            return layer;
        }

        public List<xpPublishedObject> GetTakeItemPublishedObjects(int takeID)
        {
            List<xpPublishedObject> _response = new List<xpPublishedObject>();
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        int objCount = takeItem.PublishedObjectCount;
                        for (int i = 0; i < objCount; i++)
                        {
                            xpPublishedObject publishedObject;
                            if (takeItem.GetPublishedObject(i, out publishedObject))
                            {
                                _response.Add(publishedObject);
                            }
                        }
                    }
                }
            }
            catch { return _response; }

            return _response;
        }

        public bool EditTakeItemProperty(int takeID, string objName, string propName, string value, string materialName = null)
        {
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        xpPublishedObject publishedObject;
                        if (takeItem.GetPublishedObjectByName(objName, out publishedObject))
                        {
                            int propCount = publishedObject.PropertyCount;
                            // Loop through all properties until we find the one with our selected name
                            for (int propID = 0; propID < propCount; propID++)
                            {
                                string tempName;
                                PropertyType propType;
                                publishedObject.GetPropertyInfo(propID, out tempName, out propType);
                                // Check if name is what we are looking for
                                if (tempName.Equals(propName, StringComparison.OrdinalIgnoreCase))
                                {
                                    switch (propType)
                                    {
                                        case PropertyType.pt_String:
                                            publishedObject.SetPropertyString(propID, value.Trim());
                                            break;
                                        case PropertyType.pt_Boolean:
                                            Boolean val;
                                            if (Boolean.TryParse(value.Trim(), out val))
                                                publishedObject.SetPropertyBool(propID, val);
                                            break;
                                        case PropertyType.pt_Material:
                                            int face = 0;
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                xpMaterial material;
                                                if (_engine.GetMaterialByName(value, out material))
                                                {
                                                    publishedObject.SetPropertyMaterial(propID, face, material);
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                            takeItem.UpdateThumbnail();
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }

        public bool EditProp(xpPublishedObject publishedObject, string propName, object value, string materialName = null)
        {
            try
            {
                int propCount = publishedObject.PropertyCount;
                // Loop through all properties until we find the one with our selected name
                for (int propID = 0; propID < propCount; propID++)
                {
                    string tempName;
                    PropertyType propType;
                    publishedObject.GetPropertyInfo(propID, out tempName, out propType);
                    // Check if name is what we are looking for
                    if (tempName.Equals(propName, StringComparison.OrdinalIgnoreCase))
                    {
                        string strValue = value.ToString().Trim();
                        switch (propType)
                        {
                            case PropertyType.pt_String:
                                publishedObject.SetPropertyString(propID, strValue);
                                break;
                            case PropertyType.pt_Boolean:
                                Boolean val;
                                if (Boolean.TryParse(strValue, out val))
                                    publishedObject.SetPropertyBool(propID, val);
                                break;
                            case PropertyType.pt_Material:
                                int face;
                                if (int.TryParse(strValue, out face) && !string.IsNullOrEmpty(materialName))
                                {
                                    xpMaterial material;
                                    if (_engine.GetMaterialByName(materialName, out material))
                                    {
                                        publishedObject.SetPropertyMaterial(propID, face, material);
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }

        public bool EditTakeItemProperty(int takeID, string objName, int propID, string value, string materialName = null)
        {
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        if (takeItem.GetPublishedObjectByName(objName, out xpPublishedObject publishedObject))
                        {
                            string propName;
                            PropertyType propType;
                            publishedObject.GetPropertyInfo(propID, out propName, out propType);
                            switch (propType)
                            {
                                case PropertyType.pt_String:
                                    publishedObject.SetPropertyString(propID, value.Trim());
                                    break;
                                case PropertyType.pt_Boolean:
                                    Boolean val;
                                    if (Boolean.TryParse(value.Trim(), out val))
                                        publishedObject.SetPropertyBool(propID, val);
                                    break;
                                case PropertyType.pt_Material:
                                    int face;
                                    if (int.TryParse(value.Trim(), out face) && !string.IsNullOrEmpty(materialName))
                                    {
                                        xpMaterial material;
                                        if (_engine.GetMaterialByName(materialName, out material))
                                        {
                                            publishedObject.SetPropertyMaterial(propID, face, material);
                                        }
                                    }
                                    break;
                            }
                            takeItem.UpdateThumbnail();
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }

        public bool EditTakeItemProperty(int takeID, int index, string propName, string value, string materialName = null)
        {
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        xpPublishedObject publishedObject;
                        if (takeItem.GetPublishedObject(index, out publishedObject))
                        {
                            int propCount = publishedObject.PropertyCount;
                            // Loop through all properties until we find the one with our selected name
                            for (int propID = 0; propID < propCount; propID++)
                            {
                                string tempName;
                                PropertyType propType;
                                publishedObject.GetPropertyInfo(propID, out tempName, out propType);
                                // Check if name is what we are looking for
                                if (tempName.Equals(propName, StringComparison.OrdinalIgnoreCase))
                                {
                                    switch (propType)
                                    {
                                        case PropertyType.pt_String:
                                            publishedObject.SetPropertyString(propID, value.Trim());
                                            break;
                                        case PropertyType.pt_Boolean:
                                            Boolean val;
                                            if (Boolean.TryParse(value.Trim(), out val))
                                                publishedObject.SetPropertyBool(propID, val);
                                            break;
                                        case PropertyType.pt_Material:
                                            int face;
                                            if (int.TryParse(value.Trim(), out face) && !string.IsNullOrEmpty(materialName))
                                            {
                                                xpMaterial material;
                                                if (_engine.GetMaterialByName(materialName, out material))
                                                {
                                                    publishedObject.SetPropertyMaterial(propID, face, material);
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                            takeItem.UpdateThumbnail();
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }

        public bool EditTakeItemProperty(int takeID, int index, int propID, string value, string materialName = null)
        {
            try
            {
                xpBaseTakeItem baseTakeItem;
                // Retrieve take item
                if (_engine.Sequencer.GetTakeItemByID(takeID, out baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        xpPublishedObject publishedObject;
                        if (takeItem.GetPublishedObject(index, out publishedObject))
                        {
                            string propName;
                            PropertyType propType;
                            publishedObject.GetPropertyInfo(propID, out propName, out propType);
                            switch (propType)
                            {
                                case PropertyType.pt_String:
                                    publishedObject.SetPropertyString(propID, value.Trim());
                                    break;
                                case PropertyType.pt_Boolean:
                                    Boolean val;
                                    if (Boolean.TryParse(value.Trim(), out val))
                                        publishedObject.SetPropertyBool(propID, val);
                                    break;
                                case PropertyType.pt_Material:
                                    int face;
                                    if (int.TryParse(value.Trim(), out face) && !string.IsNullOrEmpty(materialName))
                                    {
                                        xpMaterial material;
                                        if (_engine.GetMaterialByName(materialName, out material))
                                        {
                                            publishedObject.SetPropertyMaterial(propID, face, material);
                                        }
                                    }
                                    break;
                            }
                            takeItem.UpdateThumbnail();
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }

        #endregion

        #region Scenes
        public bool TakeScene(string sceneName, bool online = true, int frameBuffer = 0, int layerIndex = 0)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByName(sceneName, out _scene, false))
                    return false;
                if (online)
                    return _scene.SetOnline(frameBuffer, layerIndex);
                else
                    return _scene.SetOffline();
            }
            catch { return false; }
        }

        public bool TakeScene(int sceneID, bool online = true, int frameBuffer = 0, int layerIndex = 0)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByID(sceneID, out _scene, false))
                    return false;
                // Set the scene online or offline
                if (online)
                    return _scene.SetOnline(frameBuffer, layerIndex);
                else
                    return _scene.SetOffline();
            }
            catch { return false; }
        }

        public bool PlaySceneDirector(int takeID, string directoryName)
        {
            try
            {
                if (_engine.Sequencer.GetTakeItemByID(takeID, out xpBaseTakeItem baseTakeItem))
                {
                    if (baseTakeItem is xpTakeItem)
                    {
                        xpTakeItem takeItem = (xpTakeItem)baseTakeItem;
                        if (takeItem != null)
                        {
                            if (!_engine.GetOutputFrameBuffer(takeItem.FrameBufferIndex, out xpOutputFrameBuffer outputBuffer))
                                return false;

                            if (!outputBuffer.GetSceneOnLayer(takeItem.Layer, out xpScene _scene))
                                return false;

                            if (!_scene.GetSceneDirectorByName(directoryName, out xpSceneDirector _sceneDirector))
                                return false;

                            _sceneDirector.AutoStop = true;
                            return _sceneDirector.PlayRange(0, _sceneDirector.Duration);
                        }
                    }
                }
            }
            catch { return false; };

            return false;
        }

        public string GetSceneDirectorName(xpTakeItem takeItem, string directoryName)
        {
            try
            {
                if (takeItem != null)
                {
                    if (!_engine.GetOutputFrameBuffer(takeItem.FrameBufferIndex, out xpOutputFrameBuffer outputBuffer))
                        return "";

                    if (!outputBuffer.GetSceneOnLayer(takeItem.Layer, out xpScene _scene))
                        return "";

                    return _scene.SceneDirector.Name;
                }
            }
            catch { return ""; };

            return "";
        }

        public bool PlaySceneDirector(xpTakeItem takeItem, string directoryName)
        {
            try
            {
                if (takeItem != null)
                {
                    if (!_engine.GetOutputFrameBuffer(takeItem.FrameBufferIndex, out xpOutputFrameBuffer outputBuffer))
                        return false;

                    if (!outputBuffer.GetSceneOnLayer(takeItem.Layer, out xpScene _scene))
                        return false;

                    if (!_scene.GetSceneDirectorByName(directoryName, out xpSceneDirector _sceneDirector))
                        return false;

                    _sceneDirector.AutoStop = true;
                    return _sceneDirector.PlayRange(0, _sceneDirector.Duration);
                }
            }
            catch { return false; };

            return false;
        }

        public bool SetSceneDirector(xpTakeItem takeItem, string directoryName)
        {
            try
            {
                if (takeItem != null)
                {
                    if (!_engine.GetOutputFrameBuffer(takeItem.FrameBufferIndex, out xpOutputFrameBuffer outputBuffer))
                        return false;

                    if (!outputBuffer.GetSceneOnLayer(takeItem.Layer, out xpScene _scene))
                        return false;

                    if (!_scene.GetSceneDirectorByName(directoryName, out xpSceneDirector _sceneDirector))
                        return false;

                    _sceneDirector.Position = 0;
                    _sceneDirector.SetAsDefault();
                    return _scene.ApplySceneDirectorPositions();
                }
            }
            catch { return false; };

            return false;
        }

        public bool AnimateScene(string sceneName, string animName, bool waitFor = false, int timeOut = -1)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByName(sceneName, out _scene, false))
                    return false;
                xpAnimController _animController = null;
                // Get the animation controller
                if (!_scene.GetAnimControllerByName(animName, out _animController))
                    return false;

                return _animController.Play(waitFor, timeOut);
            }
            catch { return false; }
        }

        public bool AnimateScene(int sceneID, string animName, bool waitFor = false, int timeOut = -1)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByID(sceneID, out _scene, false))
                    return false;
                // Get the animation controller
                if (!_scene.GetAnimControllerByName(animName, out xpAnimController _animController))
                    return false;

                return _animController.Play(waitFor, timeOut);
            }
            catch { return false; }
        }

        public bool AnimateScene(string sceneName, int animID, bool waitFor = false, int timeOut = -1)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByName(sceneName, out _scene, false))
                    return false;
                // Get the animation controller
                if (!_scene.GetAnimController(animID, out xpAnimController _animController))
                    return false;

                return _animController.Play(waitFor, timeOut);
            }
            catch { return false; }
        }

        public bool AnimateScene(int sceneID, int animID, bool waitFor = false, int timeOut = -1)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByID(sceneID, out _scene, false))
                    return false;
                // Get the animation controller
                if (!_scene.GetAnimController(animID, out xpAnimController _animController))
                    return false;

                return _animController.Play(waitFor, timeOut);
            }
            catch { return false; }
        }

        #endregion

        #region EditScene
        // Edit a scene text objects by scene name
        public bool EditSceneTexts(string sceneName, Dictionary<string, string> sceneData)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByName(sceneName, out _scene, false))
                    return false;
                foreach (KeyValuePair<string, string> data in sceneData)
                {
                    try
                    {
                        xpBaseObject obj;
                        _scene.GetObjectByName(data.Key, out obj);
                        if (obj != null)
                        {
                            xpTextObject txtObj = (xpTextObject)obj;
                            if (txtObj != null)
                            {
                                txtObj.Text = data.Value;
                            }
                        }
                    }
                    catch { continue; }
                }
            }
            catch { return false; }

            return true;
        }

        // Edit a scene text objects by scene ID
        public bool EditSceneTexts(int sceneID, Dictionary<string, string> sceneData)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByID(sceneID, out _scene, false))
                    return false;
                foreach (KeyValuePair<string, string> data in sceneData)
                {
                    try
                    {
                        xpBaseObject obj;
                        _scene.GetObjectByName(data.Key, out obj);
                        if (obj != null)
                        {
                            xpTextObject txtObj = (xpTextObject)obj;
                            if (txtObj != null)
                            {
                                txtObj.Text = data.Value;
                            }
                        }
                    }
                    catch { continue; }
                }
            }
            catch { return false; }

            return true;
        }

        // Edit scenes text value by scene ID and text name
        public bool EditSceneText(string sceneName, string txtName, string value)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByName(sceneName, out _scene, false))
                    return false;

                xpBaseObject obj;
                _scene.GetObjectByName(txtName, out obj);
                if (obj != null)
                {
                    xpTextObject txtObj = (xpTextObject)obj;
                    if (txtObj != null)
                    {
                        txtObj.Text = value;
                    }
                }
            }
            catch { return false; }

            return true;
        }

        // Edit scenes text value by scene ID and text name
        public bool EditSceneText(int sceneID, string txtName, string value)
        {
            try
            {
                xpScene _scene = null;
                // Get the scene object, if we fail to get it return false
                if (!_engine.GetSceneByID(sceneID, out _scene, false))
                    return false;

                xpBaseObject obj;
                _scene.GetObjectByName(txtName, out obj);
                if (obj != null)
                {
                    xpTextObject txtObj = (xpTextObject)obj;
                    if (txtObj != null)
                    {
                        txtObj.Text = value;
                    }
                }
            }
            catch { return false; }

            return true;
        }

        #endregion

        #region Widgets

        // Edit engines counter widget by name
        public bool EditCounterWidget(string name, int value)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpCounterWidget countWidg = (xpCounterWidget)baseWidg;
                    if (countWidg != null)
                    {
                        countWidg.Value = value;
                    }
                }
            }
            catch { return false; }

            return true;
        }

        // Get the value from a counter widget by name
        public int GetCounterWidgetValue(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpCounterWidget countWidg = (xpCounterWidget)baseWidg;
                    if (countWidg != null)
                    {
                        return countWidg.Value;
                    }
                }
            }
            catch { return -1; }

            return -1;
        }

        // Set engines Text List widget index by name
        public bool SetTextListWidgetItemIndex(string name, int index)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        listWidg.ItemIndex = index;
                        return true;
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Set engines Text List widget to the next item by name
        public int TextListWidgetNext(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        listWidg.Next();
                        return listWidg.ItemIndex;
                    }
                }
            }
            catch { return -1; }

            return -1;
        }

        // Set engines Text List widget to the previous item by name
        public int TextListWidgetPrev(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        listWidg.Prev();
                        return listWidg.ItemIndex;
                    }
                }
            }
            catch { return -1; }

            return -1;
        }

        // Set engines Text List widget to the the first item by name
        public int ResetTextListWidgetIndex(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        listWidg.Reset();
                        return listWidg.ItemIndex;
                    }
                }
            }
            catch { return -1; }

            return -1;
        }

        // Set engines Text List widget to the previous item by name
        public int AddTextListWidgeString(string name, string value)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        return listWidg.AddString(value.Trim());
                    }
                }
            }
            catch { return -1; }

            return -1;
        }

        // Get the all text values from a Text List widget by name
        public bool SetTextListWidgetValues(string name, List<string> values, bool autoCreate = false)
        {
            try
            {
                _engine.GetWidgetByName(name, out xpBaseWidget baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        // Remove all old strings
                        listWidg.ClearStrings();
                        // Loop through values and add each to list
                        foreach (string value in values)
                        {
                            listWidg.AddString(value.Trim());
                        }
                        listWidg.Reset();
                    }
                }
            }
            catch { return false; }

            return true;
        }

        // Get the all text values from a Text List widget by name
        public bool SetTextListWidgetToValue(string name, string value)
        {
            int _oldIndex = 0;
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        // Log old index to reset with later
                        _oldIndex = listWidg.ItemIndex;
                        listWidg.ItemIndex = 0;

                        // Loop through values until we find the matching string
                        for (int i = 0; i < listWidg.Count; i++)
                        {
                            listWidg.ItemIndex = i;
                            if (String.Equals(listWidg.Value.Trim(), value.Trim(), StringComparison.OrdinalIgnoreCase))
                            {
                                return true;
                            }
                        }
                        // Value wasn't found, so reset to old
                        listWidg.ItemIndex = _oldIndex;
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Set engines Text List widget to the previous item by name
        public bool ClearTextListWidget(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        listWidg.ClearStrings();
                        return true;
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Get the value from a Text List widget by name
        public string GetTextListWidgetValue(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        return listWidg.Value;
                    }
                }
            }
            catch { return ""; }

            return "";
        }

        // Get the all text values from a Text List widget by name
        public List<string> GetTextListWidgetValues(string name)
        {
            List<string> _response = new List<string>();
            int _oldIndex = 0;
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        // Log old index to reset with later
                        _oldIndex = listWidg.ItemIndex;
                        listWidg.ItemIndex = 0;
                        for (int i = 0; i < listWidg.Count; i++)
                        {
                            listWidg.ItemIndex = i;
                            _response.Add(listWidg.Value.Trim());
                        }
                        listWidg.ItemIndex = _oldIndex;
                    }
                }
            }
            catch { return _response; }

            return _response;
        }

        // Get the Item Index from a Text List widget by name
        public long GetTextListWidgetItemIndex(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        return listWidg.ItemIndex;
                    }
                }
            }
            catch { return -1; }

            return -1;
        }

        // Get the Item Index from a Text List widget by name
        public long GetTextListWidgetCount(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpTextListWidget listWidg = (xpTextListWidget)baseWidg;
                    if (listWidg != null)
                    {
                        return listWidg.Count;
                    }
                }
            }
            catch { return -1; }

            return -1;
        }

        // Start a clock widget by name
        public int GetClockWidgetTimerValue(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        return clockWidg.TimerValue;
                    }
                }
            }
            catch { return 0; }

            return 0;
        }

        // Start a clock widget by name
        public string GetClockWidgetValue(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        return clockWidg.Value;
                    }
                }
            }
            catch { return "0"; }

            return "0";
        }

        // Start a clock widget by name
        public bool StartClockWidget(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        return clockWidg.Start();
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Stop a clock widget by name
        public bool StopClockWidget(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        return clockWidg.Stop();
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Stop a clock widget by name
        public bool ResetClockWidget(string name)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        return clockWidg.Reset();
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Edit a clock widgets format by name
        public bool EditClockWidgetFormat(string name, string format = "NN:SS")
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        // Make sure its a valid Xpression format
                        if (format.Equals("S") || format.Equals("SS") || format.Equals("SSS") ||
                            format.Equals("S.Z") || format.Equals("S.ZZ") || format.Equals("S.ZZZ") || format.Equals("SS.ZZZ") ||
                            format.Equals("HH:NN") || format.Equals("HH:NN:SS") || format.Equals("HH:NN:SS.ZZZ") ||
                            format.Equals("NN:SS") || format.Equals("NN:SS.ZZZ"))
                        {
                            clockWidg.Format = format;
                            return true;
                        }
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Edit a clock widgets format by name
        public bool EditClockWidgetStartTime(string name, string startTime = "")
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        clockWidg.StartAt = startTime;
                        clockWidg.Reset();
                        return true;
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Edit a clock widgets format by name
        public bool SetClockWidgetTimerValue(string name, int value)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        clockWidg.TimerValue = value;
                        return true;
                    }
                }
            }
            catch { return false; }

            return false;
        }

        // Edit a clock widgets format by name
        public bool SetClockWidgetCallback(string name, Action<int, int, int, int> callback)
        {
            try
            {
                xpBaseWidget baseWidg;
                _engine.GetWidgetByName(name, out baseWidg);
                if (baseWidg != null)
                {
                    xpClockTimerWidget clockWidg = (xpClockTimerWidget)baseWidg;
                    if (clockWidg != null)
                    {
                        if (clockWidg.SetEventMode(ClockTimerWidgetEventMode.temSeconds))
                        {
                            void OnClockChange(int Hours, int Minutes, int Seconds, int Milli)
                            {
                                callback(Hours, Minutes, Seconds, Milli);
                            }

                            clockWidg.OnChange += OnClockChange;
                            return true;
                        }
                    }
                }
            }
            catch { return false; }


            return false;
        }

        #endregion

        #region Metadata
        public bool EditMetadataAttribute(int sceneID, string attrName, string value)
        {
            try
            {

                xpScene sceneSync;
                if (!_engine.GetSceneByID(sceneID, out sceneSync, false))
                {
                    return false;
                }
                xpMetadata mData;
                if (!sceneSync.GetMetadata(out mData))
                {
                    return false;
                }
                xpAttribute _attribute = mData.GetAttribByName(attrName);
                if (_attribute != null)
                {
                    _attribute.SetString(value);
                }
                else
                {
                    mData.AddAttribute(attrName, value);
                }
            }
            catch { return false; }

            return true;
        }

        #endregion
    }
}
